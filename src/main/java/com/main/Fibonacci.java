package com.main;

import com.main.exceptions.IllegalRangeException;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * Class which provides some operations with odd, even numbers and
 * builds <strong>Fibonacci</strong> numbers with percentage calculation.
 *
 * @author Yurii Heriak
 * @version 1.0
 */
public final class Fibonacci {
    /**
     * Start of the range.
     */
    private static int start;
    /**
     * End of the range.
     */
    private static int end;
    /**
     * Set for storing Fibonacci numbers.
     */
    private static Set<Integer> fibonacci;
    /**
     * Variable for calculating percentage.
     */
    private static final int ONE_HUNDRED_PERCENT = 100;
    /**
     * Scanner instance for reading user input.
     */
    private static Scanner scanner;

    /**
     * Initialization of <code>start</code> and <code>end</code> variables.
     */
    static {
        scanner = new Scanner(System.in, "UTF-8");
        while (true) {
            System.out.print("Enter start and end of the range: ");
            start = scanner.nextInt();
            end = scanner.nextInt();
            if (start < end) {
                break;
            } else {
                try {
                    throw new IllegalRangeException("Wrong range!");
                } catch (IllegalRangeException e) {
                    System.out.println(e.getMessage());
                }
                System.out.println("\tPlease, enter correct range!\n");
            }
        }
        System.out.println();
    }

    /**
     * Private constructor which do not allows to create an object.
     */
    private Fibonacci() {
    }

    /**
     * Prints prints odd numbers from start to the end of interval
     * and even from end to start.
     */
    public static void printOENums() {
        System.out.print("Odd numbers from " + start + " to " + end + ": ");
        for (int i = start; i <= end; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
        System.out.print("\nEven numbers from " + start + " to " + end + ": ");
        for (int i = end; i >= start; i--) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    /**
     * Prints the sum of odd and even numbers.
     */
    public static void printSumOfOENums() {
        int sumOdd = 0;
        int sumEven = 0;
        for (int i = start; i <= end; i++) {
            if (i % 2 == 0) {
                sumEven += i;
            } else {
                sumOdd += i;
            }
        }
        System.out.println("Sum of odd numbers from "
                + start + " to " + end + " = " + sumOdd);
        System.out.println("Sum of even numbers from "
                + start + " to " + end + " = " + sumEven);
    }

    /**
     * Creates set of fibonacci numbers, fills it up and prints it.
     */
    public static void buildFibonacci() {
        System.out.print("\nEnter number of fibonacci (N): ");
        int n = scanner.nextInt();
        fibonacci = new TreeSet<Integer>();
        buildFibonacci(n, maxOdd(), maxEven(), fibonacci);
        System.out.println("Fibonacci from " + maxOdd() + ": " + fibonacci);
    }

    /**
     * Prints percentage of odd and even Fibonacci numbers.
     */
    public static void printPercentage() {
        if (fibonacci.size() == 0) {
            return;
        }
        int countOdd = 0;
        int countEven = 0;
        for (Integer i : fibonacci) {
            if (i % 2 == 0) {
                countEven++;
            } else {
                countOdd++;
            }
        }
        double percentOdd = ((double) countOdd / fibonacci.size())
                * ONE_HUNDRED_PERCENT;
        double percentEven = ((double) countEven / fibonacci.size())
                * ONE_HUNDRED_PERCENT;

        System.out.printf("Odd numbers = %.2f%%%n", percentOdd);
        System.out.printf("Even numbers = %.2f%%%n", percentEven);
    }

    /**
     * Finds the biggest odd number from the range.
     *
     * @return the biggest odd number
     */
    private static int maxOdd() {
        if (end % 2 == 0) {
            return end - 1;
        } else {
            return end;
        }
    }

    /**
     * Finds the biggest even number from the range.
     *
     * @return the biggest even number
     */
    private static int maxEven() {
        if (end % 2 == 0) {
            return end;
        } else {
            return end - 1;
        }
    }

    /**
     * @param n  the number of Fibonacci numbers in the set
     * @param f1 first element of the Fibonacci sequence
     * @param f2 second element of the Fibonacci sequence
     * @param s  set that will contain Fibonacci numbers
     * @return fibonacci number on each iteration
     */
    private static int buildFibonacci(final int n,
                                      final int f1,
                                      final int f2,
                                      final Set<Integer> s) {
        if (n == 1) {
            s.add(f1);
            return f1;
        } else if (n == 2) {
            s.add(f2);
            return f2;
        }
        int next = buildFibonacci(n - 1, f1, f2, s)
                + buildFibonacci(n - 2, f1, f2, s);
        s.add(next);
        return next;
    }
}
