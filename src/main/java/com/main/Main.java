package com.main;

/**
 * Class which contains the starting point of the program.
 */
public final class Main {

    /**
     * Private constructor which do not allows to create an object.
     */
    private Main() {
    }

    /**
     * Method for demonstrating Fibonacci class functionality.
     *
     * @param args command line arguments
     */
    public static void main(final String[] args) {
        Fibonacci.printOENums();
        Fibonacci.printSumOfOENums();

        Fibonacci.buildFibonacci();
        Fibonacci.printPercentage();
    }
}
