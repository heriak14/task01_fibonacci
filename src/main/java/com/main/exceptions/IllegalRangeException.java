package com.main.exceptions;

public class IllegalRangeException extends Throwable {

    public IllegalRangeException(String message) {
        super(message);
    }
}
